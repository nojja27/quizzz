package com.example.nojja.myapplication;

import java.util.ArrayList;
import java.util.Random;

public class QuizzzController {

    private ArrayList<QuizzzModel> qm_quizList;
    private ArrayList<String> s_categories;
    private JSONHandler jsonHandler;
    private int _DifficultyLevel;
    private int _numberOfQuestions;
    private Random r_nextQUestionGenerator;

    public int get_DifficultyLevel() {
        return _DifficultyLevel;
    }

    public QuizzzController(String RawJSON, int QuizzzDifficultyLevel) {
        jsonHandler = new JSONHandler(RawJSON);
        this.s_categories = jsonHandler.getQuestionCategories();
        this._DifficultyLevel = this._numberOfQuestions = QuizzzDifficultyLevel;
        this.r_nextQUestionGenerator = new Random();
    }

    public ArrayList<String> getQuizCategories() {
        return this.s_categories;
    }

    public void getQuizes(ArrayList<String> selectedCategories) {
        this.qm_quizList = jsonHandler.getQuizzzQuestions(selectedCategories, difficultyEnum.light);
    }

    public QuizzzModel getNextQuiz() {
        if (_DifficultyLevel > this.qm_quizList.size())
            _DifficultyLevel = this.qm_quizList.size();

        if (_numberOfQuestions > 0) {
            ArrayList<QuizzzModel> qm_nextQuizList = getNotSortedQuestions();
            int size = qm_nextQuizList.size();
            if (size > 0) {
                int _index = this.r_nextQUestionGenerator.nextInt(size);
                _numberOfQuestions--;

                QuizzzModel qm_nextQuestion = qm_quizList.get(this.qm_quizList.indexOf(qm_nextQuizList.get(_index)));
                qm_nextQuestion.set_sorted(true);
                return qm_nextQuestion;
            }
        }
        return null;
    }

    public int getQuizResult(ArrayList<QuizzzModel> ql_answeredQuestions) {
        int _corretcAnswers = 0;
        for (QuizzzModel qm : ql_answeredQuestions) {
            if (qm.get_correctAnswer() == qm.get_chosenAnswer())
                _corretcAnswers++;
        }

        return _corretcAnswers;
    }

    private ArrayList<QuizzzModel> getNotSortedQuestions() {
        ArrayList<QuizzzModel> qm_notSortedQuestionsList = new ArrayList<QuizzzModel>();
        for (QuizzzModel qm_Item : this.qm_quizList) {
            if (!qm_Item.get_sorted())
                qm_notSortedQuestionsList.add(qm_Item);
        }

        return qm_notSortedQuestionsList;
    }
}
