package com.example.nojja.myapplication;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.UiModeManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        TurnOFFNighMode();
        setContentView(R.layout.activity_menu);



        Button bt_NewGame = (Button) findViewById(R.id.new_game);
        final Intent i_NewGameActivity = new Intent(this, QuizActivity.class);
        final Button bt_Settings = (Button) findViewById(R.id.settings);
        final SwitchCompat switchCompat = (SwitchCompat) findViewById(R.id.ejszakai);

        final View f_FragmentContainer = findViewById(R.id.fragment_container);
        f_FragmentContainer.setVisibility(View.INVISIBLE);

        UiModeManager uiManager2 = (UiModeManager) getSystemService(Context.UI_MODE_SERVICE);
        if(uiManager2.getNightMode() == UiModeManager.MODE_NIGHT_YES)
        {
            switchCompat.setChecked(true);
        }

        bt_NewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ab = new AlertDialog.Builder(MenuActivity.this);
                ab.setTitle("Új játék");
                ab.setMessage("Felkészültél a megmérettetésre?");
                ab.setPositiveButton("IGEN!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MenuActivity.this, "Kezdődjön a játék...", Toast.LENGTH_LONG).show();
                        SetSettingsForNewGameActivity(i_NewGameActivity);

                        startActivity(i_NewGameActivity);

                    }
                });

                ab.setNegativeButton("Még nem!", null);
                ab.show();



            }
        });

        switchCompat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                UiModeManager uiManager = (UiModeManager) getSystemService(Context.UI_MODE_SERVICE);
                if (switchCompat.isChecked()==true) {
                    uiManager.enableCarMode(0);
                    uiManager.setNightMode(UiModeManager.MODE_NIGHT_YES);
                } else {
                    uiManager.disableCarMode(0);
                    uiManager.setNightMode(UiModeManager.MODE_NIGHT_NO);
                }
            }
        });




        bt_Settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set visible if invisible, set invisible if visible
                if (f_FragmentContainer.getVisibility() == View.INVISIBLE) {
                    f_FragmentContainer.setVisibility(View.VISIBLE);
                } else {
                    f_FragmentContainer.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void SetSettingsForNewGameActivity(Intent intent){
        intent.putExtra("_name", "Peti");              // Felhasználónév
        intent.putExtra("_sex", true);                  // Felhasználó neme Férfi: True , Nő: False vagy fordítva nekem mind1
        intent.putExtra("_age", 120);                   // Felhasználó életkora számérték
        intent.putExtra("_difficultylevel", 100);       // Kiválasztott nehézségi fokozat a kérdések számát adja át

//  <-------   Későbbi továbfejlesztési lehetőségek   ------->

//        intent.putExtra("_nightmode", true);            //  Nightmode Bekapcsolva: True, Kikapcsolva: False
//        intent.putExtra("_orientation", true);          //  Fekvő üzemmód Bekapcsolva: True, Kikapcsolva: False
    }

    private void LoadSettingFragment() {
        FragmentManager fm_NewGameFM = getFragmentManager();
//        FragmentManager fm_NewGameFM = getSupportFragmentManager();
        FragmentTransaction ft_NewGameFT = fm_NewGameFM.beginTransaction();
        ft_NewGameFT.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft_NewGameFT.addToBackStack(SettingsFragment.class.getName());
        Fragment fragment = new SettingsFragment();
        ft_NewGameFT.replace(R.id.fragment_container, fragment);

        ft_NewGameFT.setCustomAnimations(android.R.animator.fade_in,
                android.R.animator.fade_out);

        //ft_NewGameFT.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        ft_NewGameFT.commit();


    }


    private void TurnOFFNighMode(){

    }




}
