package com.example.nojja.myapplication;

public class QuizzzModel {

    private int _id;
    private String _question;
    private String _correctAnswer;
    private String _answerA;
    private String _answerB;
    private String _answerC;
    private String _chosenAnswer;
    private Boolean _sorted;

    public int get_id() { return _id; }
    public void set_id(int _id) { this._id = _id; }

    public String get_question() { return _question; }
    public void set_question(String _question) {
        this._question = _question;
    }

    public String get_correctAnswer() { return _correctAnswer; }
    public void set_correctAnswer(String _correctAnswer) {
        this._correctAnswer = _correctAnswer;
    }

    public String get_answerA() { return _answerA; }
    public void set_answerA(String _answerA) {
        this._answerA = _answerA;
    }

    public String get_answerB() { return _answerB; }
    public void set_answerB(String _answerB) {
        this._answerB = _answerB;
    }

    public String get_answerC() { return _answerC; }
    public void set_answerC(String _answerC) { this._answerC = _answerC; }

    public String get_chosenAnswer() { return _chosenAnswer; }
    public void set_chosenAnswer(String _chosenAnswer) { this._chosenAnswer = _chosenAnswer; }

    public Boolean get_sorted() { return _sorted; }
    public void set_sorted(Boolean _sorted) { this._sorted = _sorted; }
}
