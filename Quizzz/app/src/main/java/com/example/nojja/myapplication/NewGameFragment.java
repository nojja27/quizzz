package com.example.nojja.myapplication;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class NewGameFragment extends Fragment {
    private View rootView;
    private ArrayList<String> categories;

    onCategorySelectedListener c_Listener;

    public interface onCategorySelectedListener {
        public void onCategorySelected(String selectedCategory);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            c_Listener = (onCategorySelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onCategorySelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_newgame, container, false);

        RadioGroup rg = (RadioGroup) rootView.findViewById(R.id.radio_container);
        for (int i = 0; i < getArguments().size(); i++) {
            RadioButton radioButton = new RadioButton(getContext());
            radioButton.setText(getArguments().getString("cat" + i));
            radioButton.setId(i);
            radioButton.setTextSize(30);
            rg.addView(radioButton);
        }

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                String selectedCategory = ((RadioButton)group.getChildAt(checkedId)).getText().toString();
                c_Listener.onCategorySelected(selectedCategory);
            }
        });

        return rootView;
    }
}
