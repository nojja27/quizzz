package com.example.nojja.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

public class SettingsModel implements Parcelable {

    private String _Name;
    private boolean _Sex;
    private int _Age;
    private int _DifficultyLevel;
    private boolean _NightMode;
    private boolean _Orientation;

    public SettingsModel(String _Name, boolean _Sex, int _Age, int _DifficultyLevel, boolean _NightMode, boolean _Orientation) {
        this._Name = _Name;
        this._Sex = _Sex;
        this._Age = _Age;
        this._DifficultyLevel = _DifficultyLevel;
        this._NightMode = _NightMode;
        this._Orientation = _Orientation;
    }

    protected SettingsModel(Parcel in) {
        _Name = in.readString();
        _Sex = in.readByte() != 0;
        _Age = in.readInt();
        _DifficultyLevel = in.readInt();
        _NightMode = in.readByte() != 0;
        _Orientation = in.readByte() != 0;
    }

    public static final Creator<SettingsModel> CREATOR = new Creator<SettingsModel>() {
        @Override
        public SettingsModel createFromParcel(Parcel in) {
            return new SettingsModel(in);
        }

        @Override
        public SettingsModel[] newArray(int size) {
            return new SettingsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_Name);
        dest.writeByte((byte) (_Sex ? 1 : 0));
        dest.writeInt(_Age);
        dest.writeInt(_DifficultyLevel);
        dest.writeByte((byte) (_NightMode ? 1 : 0));
        dest.writeByte((byte) (_Orientation ? 1 : 0));
    }
}
