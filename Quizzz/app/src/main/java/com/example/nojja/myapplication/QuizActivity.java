package com.example.nojja.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class QuizActivity extends AppCompatActivity implements
        NewGameFragment.onCategorySelectedListener,
        QuizFragment.onAnswerClickListener {

    private String _Name;
    private String _selectedCategory;
    private boolean _Sex;
    private int _Age;
    private int _DifficultyLevel;
    private boolean _NightMode;
    private boolean _Orientation;

    private QuizzzController qc_Controller;
    private View f_fragmentContainer;
    private TextView tv_actualText;
    private Button bt_menu;
    private Button bt_universal;

    private NewGameFragment nfg_NewGame;
    private QuizFragment qf_QuizFragment;
    private ResultFragment rf_ResultFragment;
    private ArrayList<QuizzzModel> ql_answeredQuestions;
    private QuizzzModel qm;

    private static final Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        _Name = getIntent().getExtras().getString("_name");
        _Sex = getIntent().getExtras().getBoolean("_sex");
        _Age = getIntent().getExtras().getInt("_age");
        _DifficultyLevel = getIntent().getExtras().getInt("_difficultylevel");
//        <-------   Későbbi továbfejlesztési lehetőségek   ------->

//        _NightMode = =getIntent().getExtras().getBoolean("_nightmode");
//        _Orientation = getIntent().getExtras().getBoolean("_orientation");

        this.qc_Controller = new QuizzzController(loadJSONFromAsset(), _DifficultyLevel);
        this.tv_actualText = (TextView) findViewById(R.id.displayed_question);
        this.f_fragmentContainer = findViewById(R.id.fragment_container);
        this.bt_menu = (Button) findViewById(R.id.bactToMenu);
        this.bt_universal = (Button) findViewById(R.id.universalButton);

        this.tv_actualText.setText("Üdvözöllek: " + _Name);

        final ArrayList<String> qc_categories = this.qc_Controller.getQuizCategories();
        this.ql_answeredQuestions = new ArrayList<QuizzzModel>();

        this.nfg_NewGame = new NewGameFragment();
        Bundle categories = new Bundle();
        for (int i = 0; i < qc_categories.size(); i++)
            categories.putString("cat" + i, qc_categories.get(i));
        this.nfg_NewGame.setArguments(categories);

        addFragment(NewGameFragment.class.getName(), nfg_NewGame, "newGameFragment");


        bt_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        bt_universal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qm = new QuizzzModel();
                switch (bt_universal.getText().toString()) {
                    case "Kezdés":
                        ArrayList<String> selectedCategories = new ArrayList<String>();
                        qf_QuizFragment = new QuizFragment();

                        String s = _selectedCategory;

                        if (qm != null && qc_categories.contains(s)) {

                            selectedCategories.add(s);
                            qc_Controller.getQuizes(selectedCategories);
                            qm = qc_Controller.getNextQuiz();

                            Bundle question = new Bundle();
                            question.putString("Question", qm.get_question());
                            question.putString("Correct", qm.get_correctAnswer());
                            question.putString("answerA", qm.get_answerA());
                            question.putString("answerB", qm.get_answerB());
                            question.putString("answerC", qm.get_answerC());
                            qf_QuizFragment.setArguments(question);

                            replaceFragment(QuizFragment.class.getName(), qf_QuizFragment, "quizFragment");
                            bt_universal.setText("Következő");
                        }
                        break;

                    case "Következő":
                        qf_QuizFragment = new QuizFragment();
                        qm = qc_Controller.getNextQuiz();
                        if (qm != null) {
                            bundle_getQuestion();

                            replaceFragment(QuizFragment.class.getName(), qf_QuizFragment, "quizFragment");
                            bt_universal.setText("Következő");
                        } else {
                            bt_universal.setText("Eredmény");
                        }
                        break;

                    case "Eredmény":
                        rf_ResultFragment = new ResultFragment();
                        int result = qc_Controller.getQuizResult(ql_answeredQuestions);

                        Bundle resultBundle = new Bundle();
                        resultBundle.putString("allSize", qc_Controller.get_DifficultyLevel() + "");
                        resultBundle.putString("result", result + "");
                        rf_ResultFragment.setArguments(resultBundle);

                        replaceFragment(QuizFragment.class.getName(), rf_ResultFragment, "resutlFragment");
                        bt_universal.setText("Új játék");
                        break;

                    case "Új játék":

                        replaceFragment(QuizFragment.class.getName(), nfg_NewGame, "newGameFragment");
                        bt_universal.setText("Kezdés");
                        break;

                    default:
                }
            }
        });
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("testQuestions");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;

    }

    private void addFragment(String ClassName, Fragment newFragment, String tagName) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(ClassName);
        ft.add(R.id.fragment_container, newFragment, tagName);
        ft.commit();
    }

    private void replaceFragment(String ClassName, Fragment newFragment, String tagName) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(ClassName);
        ft.replace(R.id.fragment_container, newFragment, tagName);
        ft.commit();
    }

    @Override
    public void onCategorySelected(String selectedCategory) {
        _selectedCategory = selectedCategory;
    }

    @Override
    public void onAnswerClick(String answer) {
        String tricky = "Trükkös";
        if (qm != null) {
            qm.set_chosenAnswer(answer);
            ql_answeredQuestions.add(qm);
            if (_selectedCategory.equals(tricky)) {
                if(qm.get_chosenAnswer() == qm.get_correctAnswer()){
                    String[] toastMessages = new String[]{"Hajrá!", "Csak így tovább!", "Tudsz te jobbat is!"};
                    randomToastGenerator(toastMessages);
                }
                else{
                    String[] toastMessages = new String[]{"Béna vagy!", "Csak így tovább, ja nem..!", "Bal lábbal keltél fel?!"};
                    randomToastGenerator(toastMessages);
                }
            }
            qf_QuizFragment = new QuizFragment();
            qm = qc_Controller.getNextQuiz();
        }

        if (qm != null) {
            bundle_getQuestion();

            replaceFragment(QuizFragment.class.getName(), qf_QuizFragment, "quizFragment");
            bt_universal.setText("Következő");
        } else {
            bt_universal.setText("Eredmény");
        }
    }

    private void randomToastGenerator(String[] toastMessages){
        int randomMsgIndex = random.nextInt(toastMessages.length - 1);
        Toast.makeText(getApplicationContext(), toastMessages[randomMsgIndex], Toast.LENGTH_LONG).show();
    }

    private  void bundle_getQuestion(){
        Bundle question = new Bundle();
        question.putString("Question", qm.get_question());
        question.putString("Correct", qm.get_correctAnswer());
        question.putString("answerA", qm.get_answerA());
        question.putString("answerB", qm.get_answerB());
        question.putString("answerC", qm.get_answerC());
        qf_QuizFragment.setArguments(question);
    }
}
