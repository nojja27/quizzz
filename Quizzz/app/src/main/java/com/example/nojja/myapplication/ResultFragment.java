package com.example.nojja.myapplication;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ResultFragment extends Fragment {
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_result, container, false);

        TextView tv_result = (TextView) rootView.findViewById(R.id.user_result);

        tv_result.setText(getArguments().getString("result") + " / " + getArguments().getString("allSize"));

        return rootView;
    }
}
