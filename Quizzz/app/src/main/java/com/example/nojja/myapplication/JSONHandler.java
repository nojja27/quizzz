package com.example.nojja.myapplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

enum difficultyEnum{
    light,
    medium,
    hard
}

public class JSONHandler {
    private String _rawJSON;

    public JSONHandler(String rawJSON) {
        this._rawJSON = rawJSON;
    }

    public ArrayList<String> getQuestionCategories() {
        ArrayList<String> categories = new ArrayList<>();
        JSONObject obj = null;
        try {
            obj = new JSONObject(_rawJSON);
            Iterator<?> keys = obj.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                categories.add(key);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return categories;
    }

    public ArrayList<QuizzzModel> getQuizzzQuestions(ArrayList<String> categories, difficultyEnum selectedDifficulty) {
        ArrayList<QuizzzModel> quizList = new ArrayList<>();

        try {
            JSONObject obj = new JSONObject(_rawJSON);

            for (String category : categories) {
                JSONArray m_jArry = obj.getJSONArray(String.valueOf(category));
                int questionNumber;
                if(selectedDifficulty == difficultyEnum.light){
                    questionNumber = 5;
                }
                else if(selectedDifficulty == difficultyEnum.medium){
                    questionNumber = 7;
                }
                else{
                    questionNumber = 9;
                }
                for (int i = 0; i < questionNumber; i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    QuizzzModel quizzzModel = new QuizzzModel();
                    quizzzModel.set_id(jo_inside.getInt("id"));
                    quizzzModel.set_question(jo_inside.getString("question"));
                    quizzzModel.set_correctAnswer(jo_inside.getString("answerCorrect"));
                    quizzzModel.set_answerA(jo_inside.getString("answerA"));
                    quizzzModel.set_answerB(jo_inside.getString("answerB"));
                    quizzzModel.set_answerC(jo_inside.getString("answerC"));
                    quizzzModel.set_sorted(false);

                    quizList.add(quizzzModel);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return quizList;
    }
}
