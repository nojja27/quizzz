package com.example.nojja.myapplication;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class QuizFragment extends Fragment{
    private View rootView;

    onAnswerClickListener c_Listener;

    public interface onAnswerClickListener {
        public void onAnswerClick(String answer);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            c_Listener = (onAnswerClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onCategorySelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_quiz , container, false);

        TextView tv_Question = (TextView) rootView.findViewById(R.id.displayed_question);
        Button b_answerA = (Button) rootView.findViewById(R.id.question1);
        Button b_answerB = (Button) rootView.findViewById(R.id.question2);
        Button b_answerC = (Button) rootView.findViewById(R.id.question3);
        Button b_answerD = (Button) rootView.findViewById(R.id.question4);

        tv_Question.setText(getArguments().getString("Question"));
        b_answerA.setText(getArguments().getString("Correct"));
        b_answerB.setText(getArguments().getString("answerA"));
        b_answerC.setText(getArguments().getString("answerB"));
        b_answerD.setText(getArguments().getString("answerC"));

        b_answerA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c_Listener.onAnswerClick(((Button)v).getText().toString());
            }
        });

        b_answerB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c_Listener.onAnswerClick(((Button)v).getText().toString());
            }
        });

        b_answerC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c_Listener.onAnswerClick(((Button)v).getText().toString());
            }
        });

        b_answerD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c_Listener.onAnswerClick(((Button)v).getText().toString());
            }
        });

        return rootView;
    }
}
